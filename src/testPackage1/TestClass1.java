package testPackage1;


import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Class that contains 5 test methods
 */
public class TestClass1 {

    /** Test constant
     * {@value #testConstant} is just test constant
     */
    public static final int testConstant = 0;

    /** field name */
    private String name;

    /**
     * Constructor for creating an object with name = #name
     * @param name name which will be related to exemplar of this class
     * @see TestClass1#TestClass1()
     */
    public TestClass1(String name)  {
         this.name = name;
    }

    /**
     * Constructor for creating an object without params
     * @see TestClass1#TestClass1(String name)
     */
    public TestClass1()  {
        this.name = "Default";
    }

    /**
     * @param name name which will be added to "Hello world"
     * @return returns list of emails
     * @see TestClass1#printHello(PrintStream ps, String name)
     */
    public String hello(String name)  {
        return "Hello, "+name;
    }

    /**
     *
     * @param ps PrintStream to which the phrase "hello #name" will be printed
     * @param name
     * @see TestClass1#hello(String name)
     */
    private void printHello(PrintStream ps, String name){
        ps.println(hello(name));
    }

    /**
     * Test method in TestClass1
     * @see TestClass1#printHello(PrintStream ps, String name)
     */
    public void test(){
        printHello(System.out, this.name);
    }

    /**
     * setter for name
     * @param name new name
     */
    public void setName(String name) {
        this.name = name;
    }
}
