package testPackage1;


/**
 * Class that encapsulates TestClass1 object
 */
public class TestClass2 {

    /** TestClass1 field */
    private TestClass1 testClass1;

    /**
     * Constructor for creating new object with one param
     * @param testClass1 TestClass1 object
     */
    public TestClass2(TestClass1 testClass1){
        this.testClass1 = testClass1;
    }

    /**
     * Getter for testClass1
     * @return testClass1
     */
    public TestClass1 getTestClass1() {
        return testClass1;
    }

    /**
     * Setter for TestClass1 field
     * @param testClass1 new TestClass1 object
     */
    public void setTestClass1(TestClass1 testClass1) {
        this.testClass1 = testClass1;
    }

    /**
     * Method for changing name in testClass1
     * @param newName new name for testClass1 object
     */
    public void changeTestClass1Name(String newName){
        testClass1.setName(newName);
    }

    /**
     * Test for TestClass2
     * @see TestClass1#test()
     */
    public void test(){
        testClass1.test();
    }


}
