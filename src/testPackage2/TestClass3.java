package testPackage2;


import testPackage1.TestClass1;
import testPackage1.TestClass2;

/**
 * Class that is placed in another package
 */
public class TestClass3 {

    /** TestClass2 field */
    private TestClass2 testClass2;

    /**
     * Constructor for creating new object with one param
     * @param testClass2 TestClass1 object
     */
    public TestClass3(TestClass2 testClass2){
        this.testClass2 = testClass2;
    }

    /**
     * Getter for testClass2
     * @return testClass2
     */
    public TestClass2 getTestClass2() {
        return testClass2;
    }

    /**
     * Setter for TestClass2 field
     * @param testClass2 new TestClass1 object
     */
    public void setTestClass2(TestClass2 testClass2) {
        this.testClass2 = testClass2;
    }

    /**
     * method for test in TestClass3
     * @see TestClass2#test()
     */
    public void test() {
        System.out.println("Hello here!");
        testClass2.test();
    }

    public static void main(String[] args) {
        TestClass3 t = new TestClass3(new TestClass2(new TestClass1("dasdas")));
        t.test();
    }


}
